<?php
register_nav_menus(
    array(
        'acessibilidade' => __('Barra de Acessibilidade'),
        'servicos'       => __('Barra de Serviços'),
        'campi'          => __('Lista de Campi'),
        'relevancia'     => __('Menu de Relevância'),
        'principal'      => __('Menu Principal')
    )
);
