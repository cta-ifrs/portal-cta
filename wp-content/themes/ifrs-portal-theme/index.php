<?php get_header(); ?>

<div class="alert alert-danger">
    <p><strong><?php _e('Erro!'); ?></strong>&nbsp;<?php _e('Template n&atilde;o encontrado.'); ?></p>
</div>

<?php get_footer(); ?>
