<ul class="menu-atalhos ">
    <li><a href="#inicio-conteudo" accesskey="1"><?php _e('Ir para o conteúdo'); ?>&nbsp;<span class="badge badge-secondary">1</span></a></li>
    <li><a href="#inicio-menu" accesskey="2"><?php _e('Ir para o menu'); ?>&nbsp;<span class="badge badge-secondary">2</span></a></li>
    <li><a href="#search-field" accesskey="3"><?php _e('Ir para a busca'); ?>&nbsp;<span class="badge badge-secondary">3</span></a></li>
    <li><a href="#inicio-rodape" accesskey="4"><?php _e('Ir para o rodapé'); ?>&nbsp;<span class="badge badge-secondary">4</span></a></li>
</ul>
