window.$ = window.jQuery = require('jquery');
window.Popper = require('popper');
require('bootstrap');

require('./modules/datatables-config');
require('./modules/documentos');
require('./modules/fancybox-config');
require('./modules/lazysizes-config');
require('./modules/menu');
require('./modules/picturefill-config');
