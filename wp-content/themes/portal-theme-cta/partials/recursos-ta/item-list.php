<div class="row">
    <div class="col-12">
        <a href="<?php the_permalink(); ?>">
        <?php if (has_post_thumbnail()) : ?>
            <?php the_post_thumbnail('post-thumbnail', array('class' => 'img-fluid noticia__img')); ?>
        <?php else : ?>
            <img src="<?php echo get_template_directory_uri(); ?>/img/noticia-placeholder-<?php echo mt_rand(0, 9); ?>.png" alt="<?php the_title(); ?>" class="img-fluid noticia__img"/>
        <?php endif; ?>
        </a>
        <h2 class="noticia__titulo"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php
            $cat = get_the_terms($post->ID, 'categorias-ta');
            $cat_name = $cat[0]->name;
            $cat_ID = $cat[0]->term_id;
        ?>
        <p class="noticia__meta">
            <span class="noticia__cartola">
                <?php echo $cat_name; ?>
            </span>
        </p>
    </div>
</div>
