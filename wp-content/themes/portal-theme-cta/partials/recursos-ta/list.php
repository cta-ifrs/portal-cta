<?php

    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'recurso-ta',
        'posts_per_page' => 16,
        'paged' => $paged,
    );
    $tags = get_tags();
    $tags_selected = array();
    /*
     * Tags são as que já estão cadastradas e o usuário
     * só selecionou no formulário, os terms são os
     * termos que ele digitou e que serão procurados
     * dentro do corpo do texto/título das TAs
     */
    $terms_selected = array();
    if(isset($_POST)){
        if(!empty($_POST['filter_recursos'])) {
            $args['tax_query'] = array();
            // filtro por categoria
            if(!empty($_POST['filter_categorias_ta']) && $_POST['filter_categorias_ta'] != 'Todas') {
                $category = $_POST['filter_categorias_ta'];
                $args['tax_query'][] =
                    array(
                        'taxonomy' => 'categorias-ta', //or tag or custom taxonomy
                        'field' => 'id',
                        'terms' => array($category)
                    );
            }

            // filtro por tag/termos
            if(!empty($_POST['filter_tags'])){
                $tags_selected = $_POST['filter_tags'];
                $final_tags = '';
                foreach($tags_selected as $tag){
                    if(array_search($tag, array_column($tags, 'name')) !== false)
                        $final_tags .= $tag.'+';
                    else
                        $terms_selected[] = $tag;
                }
                if($final_tags != '') {
                    $final_tags = trim($final_tags, '+');
                    $args['tag'] = $final_tags;
                }
                if(count($terms_selected)){
                    $args['s'] = implode('+', $terms_selected);
                }
            }
        }
    }
    $sql = repositorio_query_construct($args);
    $pageposts = $wpdb->get_results($sql, OBJECT);
    $tags_selected_ids = array();
    foreach($pageposts as $post){
        if(!isset($post->tags))
            continue;
        $tags_post = explode(',', $post->tags);
        foreach($tags_post as $tag_post){
            if(!in_array(trim($tag_post), $tags_selected_ids))
                $tags_selected_ids[] = trim($tag_post);
        }
    }
    if(!empty($tags_selected_ids)){
        $sql = repositorio_query_similar($tags_selected_ids);
        $similares = $wpdb->get_results($sql, OBJECT);
    }else{
        $similares = array();
    }
?>

<div class="lista-noticias">
    <div class="row">
        <div class="col-12">
            <h2 class="lista-noticias__title">
                Repositório de TAs
            </h2>
        </div>
        <div class="col-sm-12">
            <form action="#" method="post">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label for="tag-search" class="control-label">Palavras chave</label>
                        <select class="form-control" id="tag-search"
                                name="filter_tags[]" multiple>
                            <?php
                            if ( $tags ) :
                                foreach ( $tags as $tag ) : ?>
                                    <option <?= in_array($tag->name, $tags_selected) ? 'selected' : ''; ?> value="<?= $tag->name; ?>"><?php echo esc_html( $tag->name ); ?></option>
                                <?php endforeach;
                                foreach($terms_selected as $term): ?>
                                    <option selected value="<?= $term; ?>"><?= $term; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-sm-6 form-group">
                        <?php
                        $cat_args = array(
                            'orderby'       => 'term_id',
                            'order'         => 'ASC',
                            'hide_empty'    => false, // todo: verificar
                        );
                        $cats = get_terms('categorias-ta', $cat_args);
                        ?>
                        <label for="categories" class="control-label">Categoria</label>
                        <select class="form-control" name="filter_categorias_ta"
                            id="categories">
                            <option>Todas</option>
                            <?php foreach($cats as $cat): ?>
                                <?php $selected = $cat->term_id == $category ? 'selected' : ''; ?>
                                <option value="<?= $cat->term_id; ?>" <?= $selected; ?>>
                                    <?= $cat->name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <input type="submit" name="filter_recursos" value="Buscar" id="buscar">
            </form>
            <?php if(!empty($similares)){ ?>
                <span class="tag-recomendacoes">
                    <b>Você também pode pesquisar por: </b>
                    <?php foreach($similares as $i => $tag): ?>
                        <a href="#" class="tag-recomendacao" data-name="<?= $tag->name; ?>"><?= $tag->name;?></a>
                        <?php if($i < count($similares) - 1) echo '-'; ?>
                    <?php endforeach; ?>
                </span>
            <?php } ?>
        </div>
    </div>
    <HR>
    <div class="card-columns lista-noticias__content">
    <?php if ($pageposts): ?>
    <?php global $post; ?>
    <?php foreach ($pageposts as $post): ?>
        <?php setup_postdata($post); ?>
        <div class="card border-light">
            <div class="card-body p-0">
                <article class="noticia">
                    <?php get_template_part('partials/recursos-ta/item-list'); ?>
                </article>
            </div>
        </div>
    <?php endforeach; ?>
    <?php endif; ?>
    </div>

    <div class="row">
        <div class="col-12">
            <nav class="text-center">
                <?php echo portal_pagination(); ?>
            </nav>
        </div>
    </div>
</div>
