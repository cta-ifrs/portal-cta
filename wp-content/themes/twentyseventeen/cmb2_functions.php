<?php

if ( file_exists(  __DIR__ . '/../../plugins/cmb2/init.php' ) ) {
    require_once __DIR__ . '/../../plugins/cmb2/init.php';
    // hook the function to the cmb2_init action
    add_action( 'cmb2_init', 'infos_recursos_ta' );

    // create the function that creates metaboxes and populates them with fields
    function infos_recursos_ta() {

        // set the prefix (start with an underscore to hide it from the custom fields list
        $prefix = '_infos_recurso_ta_';

        // create the metabox
        $cmb = new_cmb2_box( array(
            'id'            => 'infos_recursos_ta',
            'title'         => 'Informações do Recurso de TA',
            'object_types'  => array( 'recurso-ta' ), // post type
            'context'       => 'advanced', // 'normal', 'advanced' or 'side'
            'priority'      => 'high', // 'high', 'core', 'default' or 'low'
            'show_names'    => true, // show field names on the left
        ) );

        // galeria de imagens
        $cmb->add_field( array(
            'name' => 'Imagens',
            'desc' => 'Galeria de imagens do Recurso de TA',
            'id'   => $prefix.'imagens',
            'type' => 'file_list',
            'query_args' => array( 'type' => 'image' ), // Only images attachment
            'text' => array(
                'add_upload_files_text' => 'Carregar Imagens',
                'remove_image_text' => 'Remover Imagens',
                'file_text' => 'Imagem:',
                'file_download_text' => 'Baixar',
                'remove_text' => 'Remover',
            ),
        ) );

        // arquivos
        $cmb->add_field( array(
            'name' => 'Arquivos',
            'desc' => 'Manuais, artes, etc.',
            'id'   => $prefix.'arquivos',
            'type' => 'file_list',
            'text' => array(
                'add_upload_files_text' => 'Carregar Arquivos',
                'remove_image_text' => 'Remover Arquivos',
                'file_text' => 'Arquivo: ',
                'file_download_text' => 'Download',
                'remove_text' => 'Remover',

            ),
        ));


        //Vídeos
        $cmb->add_field(array(
            'name'    => 'Vídeos',
            'desc'    => 'Links do YouTube',
            'repeatable' => true,
            'id'      => $prefix.'videos',
            'type'    => 'oEmbed',
            'text' => array(
                'add_row_text' => "+ Vídeo"
            ),
        ));

    }

    /**
     * Sample template tag function for outputting a cmb2 file_list
     *
     * @param  string  $file_list_meta_key The field meta key.
     * @param  string  $title              Title of the display
     * @param  string  $img_size           Size of image to show
     */
    function cmb2_output_file_list( $file_list_meta_key, $title, $img_size = 'medium' ) {

        // Get the list of files
        $files = get_post_meta( get_the_ID(), $file_list_meta_key, 1 );
        echo '<h1>'.$title.'</h1>';
        echo '<div class="file-list-wrap">';
        // Loop through them and output an image
        foreach ( (array) $files as $attachment_id => $attachment_url ) {
            echo '<div class="file-list-image">';
            echo wp_get_attachment_image( $attachment_id, $img_size );
            echo '</div>';
        }
        echo '</div>';
    }


}