<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	if ( is_sticky() && is_home() ) :
		echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
	endif;
	?>
	<header class="entry-header">
		<?php
		if ( 'post' === get_post_type() ) {
			echo '<div class="entry-meta">';
				if ( is_single() ) {
					twentyseventeen_posted_on();
				} else {
					echo twentyseventeen_time_link();
					twentyseventeen_edit_link();
				};
			echo '</div><!-- .entry-meta -->';
		};

		if ( is_single() ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} elseif ( is_front_page() && is_home() ) {
			the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		} else {
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		}
		?>
	</header><!-- .entry-header -->

	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail( 'twentyseventeen-featured-image' ); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );

		########## RECURSOS TA ##############
        $videos = get_post_meta( get_the_ID(), '_infos_recurso_ta_videos', true );
        if(!empty($videos)){
            echo '<h1>Vídeos</h1>';
            foreach($videos as $id => $url){
                echo $url;
                //echo wp_oembed_get( $url );
            }
        }

        cmb2_output_file_list( '_infos_recurso_ta_imagens', 'Galeria de Imagens');

        $arquivos = get_post_meta( get_the_ID(), '_infos_recurso_ta_arquivos', true);
        if(!empty($arquivos)){
            echo '<h1>Arquivos</h1><ul>';
            foreach($arquivos as $id => $url){
                $name = basename(get_attached_file( $id ));
                echo '<li><a target="blank" href="'.$url.'">'.$name.'</a></li>';
            }
            echo '</ul>';
        }
        ?>
        <h2>Tags</h2>
        <ul>
            <?php
            $tags = get_tags();
            if ( $tags ) :
                foreach ( $tags as $tag ) : ?>
                    <li><a href="<?php echo esc_url( get_tag_link( $tag->term_id ) ); ?>" title="<?php echo esc_attr( $tag->name ); ?>"><?php echo esc_html( $tag->name ); ?></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <?php
        ########## RECURSOS TA #############

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
	if ( is_single() ) {
		twentyseventeen_entry_footer();
	}
	?>

</article><!-- #post-## -->
